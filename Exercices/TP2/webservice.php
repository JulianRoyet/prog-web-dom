
<?php
    require_once("structure.php");
    require_once("tp2-helpers.php");
    require_once("geocodage.php");

    $top = $_GET["top"];
    $lon = $_GET["lon"];
    $lat = $_GET["lat"]; 
    
    //printf("%s, %f, %f<br>",$top, $lon, $lat);

    // Now let's create the CSV to read data from
    $csv = new CSV(["name", "adr", "lon", "lat"]);
    $csv->readGeoJSON("borneswifi_EPSG4326.json", ["features"], ["name"=>["properties", "AP_ANTENNE1"], "adr"=>["properties", "Antenne 1"], "lon"=>["properties", "longitude"], "lat"=>["properties", "latitude"]]);

    $a = array('lon'=>$lon, 'lat'=>$lat);


    function topN($N, $coord, $data){

        $lines = $data->all_lines();

        for($i = 0; $i < $data->size(); $i++){
            $dis = distance($coord, $lines[$i]);
            $lines[$i]["dist"] = $dis;
        }

        usort($lines, function($a, $b){
            return $a["dist"] <=> $b["dist"];
        });
        
        $topN = array_slice($lines, 0, $N);
        return $topN;
    }

    $topN = topN($top, $a, $csv);
    $json = json_encode($topN);
    printf("%s", $json);
?>
