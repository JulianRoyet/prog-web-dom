<?php
if ($argc != 2)
{
    printf("Usage : php comptage.php <fichier>\n");
    return;
}

$lines = file($argv[1]);
printf("Number of outposts: %d\n", count($lines));
?>