<head>
    <title>clientwebservice</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="clientwebservice.css">
</head>
<body>

    <form method="get" action="clientwebservice.php">
        <label for="N">Nombre de points d'accès</label> <input type="text" id="N" name="N"/> <br />
        <label for="longitude">Longitude</label> <input type="text" id="longitude" name="longitude"/> <br />
        <label for="latitude">Latitude</label> <input type="text" id="latitude" name="latitude"/> <br />
        <input type="submit" value="submit"/>
    </form>

    <table>
        <thread>
            <tr>
                <?php
                    printf("<th>Nom</th>\n");
                    printf("<th>Adresse</th>\n");
                    printf("<th>Longitude</th>\n");
                    printf("<th>Latitude</th>\n");
                ?>
            </tr>
        </thread>
        <tbody>
            <?php
                if(isset($_GET["N"]) && isset($_GET["longitude"]) && isset($_GET["latitude"])){
                    $curl = curl_init();

                    $top=$_GET["N"];
                    $lati=$_GET["latitude"];
                    $long=$_GET["longitude"];

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://localhost/ProgWeb/prog-web-dom/Exercices/TP2/webservice.php?top=$top&lat=$lati&lon=$long",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_HTTPHEADER => array(
                            "cache-control: no-cache"
                        ),
                    ));
            
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
            
                    curl_close($curl);
                    $response = json_decode($response, true);
                    for ($k = 0 ; $k < $top ; $k ++){
                        $name = $response[$k]["name"];
                        $adresse = $response[$k]["adr"];
                        $longitude = $response[$k]["lon"];
                        $latitude = $response[$k]["lat"];
                        printf("<tr>\n");
                        printf("<th>%s</th>\n", $name);
                        printf("<th>%s</th>\n", $adresse);
                        printf("<th>%s</th>\n", $latitude);
                        printf("<th>%s</th>\n", $longitude);
                        printf("</tr>\n");
                    }
                }
            ?>
        </tbody>
    </table>

</body>