<?php

class CSV{
    private array $column_names;
    private array $columns;
    private array $data;

    public function __construct($cols){
        $this->column_names = array();
        for($i = 0; $i < count($cols); $i++){
            $this->column_names[$cols[$i]] = $i;
        }
        $this->data = array();
        $this->columns = array();
    }


    public function readCSV($filename){
        if (($handler = fopen($filename, "r")) !== FALSE) {
            for($row = 1; (($line = fgetcsv($handler, 1000, ",")) !== FALSE) ; $row++) {
                $this->add_line($line);
            }
            fclose($handler);
        }
    }

    public function readGeoJSON($filename, $list_pos, $format){
        function extract_format($json, $format){
            $out = $json;
            foreach($format as $step){
                $out = $out[$step];
            }
            return $out;
        }

        if (($json = file_get_contents($filename)) !== FALSE) {
            $json = json_decode($json, true);
            $json = extract_format($json, $list_pos);

            $c = count($json);
            for($row = 0; $row < $c ; $row++) {
                $line = array_fill(0, count($this->column_names), "");
                
                foreach($this->column_names as $key => $idx){
                    $line[$idx] = extract_format($json[$row], $format[$key]);
                }

                $this->add_line($line);
            }
        }
    }

    public function add_line($line){
        $idx = count($this->data);
        array_push($this->data, array());
        
        for ($c=0; $c < count($this->column_names); $c++) {
            if($c < count($line)){
                array_push($this->data[$idx], $line[$c]);

                if(!isset($this->columns[$c][$line[$c]])){
                    $this->columns[$c][$line[$c]] = array();
                }
                
                array_push($this->columns[$c][$line[$c]], $idx);
            }
            else{
                array_push($this->data[$idx], "NULL");
                if(!isset($this->columns[$c][$line[$c]])){
                    $this->columns[$c]["NULL"] = array();
                }
                
                array_push($this->columns[$c]["NULL"], $idx);
            }
        }
    }

    public function data(){
        return $this->data;
    }
    public function size(){
        return count($this->data);
    }
    public function columns($cols){
        $out = array();
        foreach ($cols as $name) {
            $out[$name] = array();
        }
        for($i=0; $i<count($this->data); $i++){
            foreach ($cols as $name) {
                array_push($out[$name], $this->data[$i][$this->column_names[$name]]);
            }
        }
        return $out;
    }
    public function get_line($idx){
        $out = array();
        foreach($this->column_names as $name=>$i){
            $out[$name] = $this->data[$idx][$i];
        }
        return $out;
    }

    public function all_lines(){
        $out = array();
        for($i=0; $i<count($this->data); $i++){
            array_push($out, array());
            foreach ($this->column_names as $name=>$cid) {
                $out[$i][$name] = $this->data[$i][$cid];
            }
        }
        return $out;
    }

    public function get_lines($cols){
        $out = array();

        $lines = array();
        $first = true;
        foreach($cols as $col => $val){    
            if(isset($this->column_names[$col])){
                if($first){
                    $lines = $this->columns[$this->column_names[$col]][$val];
                    $first = false;
                }
                else{
                    $lines = array_intersect($lines, $this->columns[$this->column_names[$col]][$val]);
                }
            }
        }

        for($i = 0; $i < count($lines); $i++){
            array_push($out, $this->get_line($lines[$i]));
        }
        return $out;
    }

    public function get_lines_filter($filters){
        $out = array();

        $lines = array();
        
        for($i=0; $i<count($this->data); $i++){
            $inc = true;
            foreach($filters as $col => $filter){    
                if(isset($this->column_names[$col])){
                    if(!call_user_func($filter, $this->data[$i][$this->column_names[$col]])){
                        $inc = false;
                        break; 
                    }
                }
            }
            if($inc){
                array_push($out, $this->get_line($i));
            }
        }

        for($i = 0; $i < count($lines); $i++){
            array_push($out, $this->get_line($lines[$i]));
        }
        return $out;
    }
}
?>