
<?php
    require_once("structure.php");
    require_once("tp2-helpers.php");
    require_once("geocodage.php");

    $top = $_GET["top"];
    $lon = $_GET["lon"];
    $lat = $_GET["lat"];
    $op = false;
    if(isset($_GET["op"])){
        $op = $_GET["op"];
    }
    
    
    //printf("%s, %f, %f<br>",$top, $lon, $lat);

    // Now let's create the CSV to read data from
    $csv = new CSV(["op", "tech", "lon", "lat", "adr"]);
    $csv->readGeoJSON("GSM.json", ["features"], ["op"=>["properties", "OPERATEUR"], 
                                                 "adr"=>["properties", "ANT_ADRES_LIBEL"],
                                                 "tech" => ["properties", "ANT_TECHNO"],
                                                 "lon"=>["geometry", "coordinates", 0],
                                                 "lat"=>["geometry", "coordinates", 1]]);

    $a = array('lon'=>$lon, 'lat'=>$lat);


    function topN($N, $coord, $data, $op){

        $lines = $data->all_lines();

        for($i = 0; $i < $data->size(); $i++){
            $dis = distance($coord, $lines[$i]);
            $lines[$i]["dist"] = $dis;
        }

        usort($lines, function($a, $b){
            return $a["dist"] <=> $b["dist"];
        });

        if($op != false){
            $lines = array_filter($lines, function($x) use($op){
                return $x["op"] == $op;
            });
        }

        $topN = array_slice($lines, 0, $N);
        return $topN;
    }

    $topN = topN($top, $a, $csv, $op);
    $json = json_encode($topN);
    printf("%s", $json);
?>
