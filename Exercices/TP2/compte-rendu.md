% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : Points d'accès wifi

## Participants 

* Royet Julian
* Conjard Samuel
* Capet Théo

## Filtres Unix

0) Utilisation des de cat en ligne de commande

1) On a enlevé la premièe ligne obselète à l'aide de nano puis on utilise la commande "cat borneswifi_EPSG4326.csv | iconv -f latin1 -t utf8 | wc -l" qui nous donne 68 points d'accès wifi.

2) Pour trouver le nombre d'emplacement différents comptenant des points d'accès wifi on utilise la commande:
    sort borneswifi_EPSG4326.csv | grep -o -E '(,[0-9]{1,2}.[0-9]{6}){1,2}' | uniq -c | wc -l
on obtient alors 54 lieux différents. Cette commande nous donne également le nombre de points d'accès à chaque coordonées (lat|long) et on obtient que le max est 5 en utilisant la commande sort. Puis on utilise: 
    cat borneswifi_EPSG4326.csv | grep \`sort borneswifi_EPSG4326.csv | grep -o -E '(,[0-9]{1,2}.[0-9]{6}){1,2}' | uniq -c | sort | tail -n 1 | cut -c 10-`
pour récupérer le nom du lieu avec le plus de points d'accès wifi: la bibliothèque.

## Traitement PHP

3) Il suffit d'utiliser la fonction count() qui permet de compter le nombre de lignes et donc qui nous retourne le nombre de points d'accès. 

4) La structure utilisée est probablement un peu plus compliquée que nécéssaire, il s'agit d'une classe personalisée vaguement inspirée des bases de données SQL. Elle permet de lire plusieurs formats de fichiers, de récupérer les données sous la forme d'une série de colonnes, ou d'une série de lignes, de rechercher certaines lignes, avec des noms fix ou des fonctions filtres custom...

5) Il y 7 points d'accès à moins de 200m. Le premier est le 1er étage du 8 PLace Grenette à une distance de 20.1m. Pour cet algorithme, il suffit de comparer les distance à l'aide de la fonction array_filter

6) Pour avoir les N premiers, on trie le tableau avec la fonction usort, puis on sélectionne les N premiers avec array_slice.

7) Dans cet exercice on réalise une requête au service web à l'aide de curl. Puis on utilise json_decode pour convertir un string json en tableau php à la sortie de la requête web.

8) Rien de vraiment nouveau dans cette question mis à part l'utilisation de json_encode

9) Pour cette question a créer une fonction pour pouvoir lire les données.

10) Il sagit d'un tableau html classique qui reprend le principe des fonctions codées précédemment. 

## Antenne GSM

1) Il y a en tout 100 antennes référencées. Il contient en supplément un ID d'antenne, un ID d'adresse, antenne microcelle ?, le nom de l'opérateur, la technologie d'antenne, le numéro cartoradio, le numéro support et est-ce que oui ou non elle supporte la technologie 2G, 3G ou 4G. 

2) 
    * Bouygues Télécom (BYG): 26 antennes
    * Orange (ORA): 26 antennes
    * SFR (SFR): 30 antennes
    * FREE (FREE): 18 antennes
Etant donné que le fichier est très petit on utilise un éditeur de texte le Ctrl+F.

3) Oui, puisque les fichier kml utilisent le format xml. On peut facilement le valider avec la commande xmllint gsm.kml --valid.

4) C'est fichier qui est difficile à lire à utiliser. Il est aussi très peu efficace en terme de stockage avec beaucoup de données redondantes. Le fichier gsm.csv ne pèse que 9115 octets alors que gsm.kml pèse 179 852 octets. Il est donc presque 20 fois plus gros à données équivalentes.

5) Il suffit de modifier quelques éléments des fonctions précédentes. On prend les données au format json car il manque les coordonnées dans le format csv.


