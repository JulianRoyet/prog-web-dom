<?php
    require_once("structure.php");
    require_once("tp2-helpers.php");
    require_once("geocodage.php");

    $N = $argv[1];



    $csv = new CSV(["name", "adr", "lon", "lat"]);
    $csv->readCSV("borneswifi_EPSG4326.csv");

    $lines = $csv->all_lines();

    for($i = 0; $i < $csv->size(); $i++){
        $dis = distance(["lon"=>"5.72752", "lat"=>"45.19102"], $lines[$i]);
        $lines[$i]["dist"] = $dis;
    }

    usort($lines, function($a, $b){
        return $a["dist"] <=> $b["dist"];
    });

    printf("Antennes à moins de 200m:\n");
    printf("=========================================\n");
    $dis200 = array_filter($lines, function($a){
        return $a["dist"] <= 200.0;
    });
    var_dump($dis200);
    printf("=========================================\n\n\n\n");

    printf("TOP %d:\n", $N);
    printf("=========================================\n");
    $topN = array_slice($lines, 0, $N);
    var_dump($topN);
    printf("=========================================\n\n\n\n");
    
    printf("Geo code top 1:\n");
    printf(getGeo($topN[0]["lon"], $topN[0]["lat"]));
    printf("\n");
    
    //var_dump($topN);
    
?>