<?php
    require_once("structure.php");
    require_once("tp2-helpers.php");



    function getGeo($lon, $lat){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-adresse.data.gouv.fr/reverse/?lat=$lat&lon=$lon",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($response, true); //because of true, it's in an array

        return $response["features"][0]["properties"]["label"];
    }
?>