<html>
    <head>
        <title>Question9-Rebond</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../TP2/clientwebservice.css">
    </head>
    <body>
        <?php
            require_once("tp3-helpers.php");
            $tab = tmdbget("person/".$_GET["actorid"],['append_to_response' => 'credits']);
            $processingTab = json_decode($tab, true);

            if(!isset($processingTab["name"]))
            {
                printf("<p>Either this actor does not exist, or they doesn't have a name.</p>");
                return;
            }
            $name = $processingTab["name"];

            printf("<h1>Filmography of ".$name."</h1>");
            if(!isset($processingTab["credits"]["cast"][0]))
            {
                printf("<p>Wierd, it seems that ".$name." was an actor in no movies?\n</p>");
                return;
            }
            

            
            printf("<table><thread><tr><th>Film</th>\n<th>Rôle</th>\n</tr></thread><tbody>");

            for($i = 0; isset($processingTab["credits"]["cast"][$i]); $i++)
            {
                $movie = $processingTab["credits"]["cast"][$i]["original_title"];
                $character = $processingTab["credits"]["cast"][$i]["character"];
                printf("<tr>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",$movie, $character);
            }

        ?>

    </body>
</html>