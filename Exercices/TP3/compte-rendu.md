% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : 1

## Participants 

* Théo Capet
* Julian Royet
* Samuel Conjard

## Mise en jambes

1) Le format de réponse est en JSON. Le film est Fight Club. Le paramètre "language=fr" nous renvoie vers une traduction française de certaines informations (genre du film, résumé, etc.).

2) cf. mise_en_jambe.php.

9) Grâce à cette discussion : https://www.themoviedb.org/talk/51ef18b5760ee3182d125573, et donc à cette foçon de construire la requête : https://api.themoviedb.org/3/person/###?api_key=###&append_to_response=credits, transformer chaque acteur en lien-rebond donnant la liste des films auxquels il a participé, ainsi que le nom du rôle est simple à réaliser.