<?php
    require_once("tp3-helpers.php");
?>

<html>
    <head>
        <title>Question10</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../TP2/clientwebservice.css">
    </head>
    <body>

    <form method="get" action="question10.php">
            <label for="id">Veuillez rentrer l'identifiant d'un film. Par exemple, l'identifiant de Fight Club est le 550. (Pour les trailers, 15 et 12 sont des bons films).</label> <input type="text" id="id" name="id"/> <br />
            <div>
            <input type="radio" id="vo" name="language" value="vo">
            <label for="vo">Langue originale</label>
            </div><div>
            <input type="radio" id="ve" name="language" value="ve">
            <label for="ve">Version anglaise</label>
            </div><div>
            <input type="radio" id="vf" name="language" value="vf">
            <label for="vf">Version française</label>
            </div>
            <input type="submit" value="Valider"/>
        </form>

            <?php
                if(isset($_GET["id"]))
                {
                    $tab1 = tmdbget("movie/".$_GET["id"]);
                    $tab2 = tmdbget("movie/".$_GET["id"],['language' => 'en']);
                    $tab3 = tmdbget("movie/".$_GET["id"],['language' => 'fr']);

                    $processingTab1 = json_decode($tab1, true);
                    if(!isset($processingTab1["title"]))
                    {
                        printf("Apparemment, l'identifiant que vous avez entré ne correspond pas à un film, ou bien ce dernier n'a pas de titre.\n");
                        return;
                    }

                    $processingTab2 = json_decode($tab2, true);
                    $processingTab3 = json_decode($tab3, true);

                    printf("<table><thread><tr><th></th>");
                    printf("<th>Version originale</th>\n");
                    printf("<th>Version anglaise</th>\n");
                    printf("<th>Version française</th></tr></thread><tbody>\n");

                    printf("<tr>\n");
                    printf("<th>Title</th>");
                    printf("<td>%s</td>\n",$processingTab1["title"]);
                    printf("<td>%s</td>\n",$processingTab2["title"]);
                    printf("<td>%s</td>\n",$processingTab3["title"]);
                    printf("</tr>\n");

                    printf("<tr>\n");
                    printf("<th>Original title</th>");
                    printf("<td>%s</td>\n",$processingTab1["original_title"]);
                    printf("<td>%s</td>\n",$processingTab2["original_title"]);
                    printf("<td>%s</td>\n",$processingTab3["original_title"]);
                    printf("</tr>\n");


                    if(isset($processingTab1["tagline"]))
                    {
                        printf("<tr>\n");
                        printf("<th>Tagline</th>");
                        printf("<td>%s</td>\n",$processingTab1["tagline"]);
                        printf("<td>%s</td>\n",$processingTab2["tagline"]);
                        printf("<td>%s</td>\n",$processingTab3["tagline"]);
                        printf("</tr>\n");
                    }

                    printf("<tr>\n");
                    printf("<th>Overview</th>");
                    printf("<td>%s</td>\n",$processingTab1["overview"]);
                    printf("<td>%s</td>\n",$processingTab2["overview"]);
                    printf("<td>%s</td>\n",$processingTab3["overview"]);
                    printf("</tr>\n");

                    printf("<tr>\n");
                    printf("<th>Home Page</th>");
                    printf("<td>%s</td>\n",$processingTab1["homepage"]);
                    printf("<td>%s</td>\n",$processingTab2["homepage"]);
                    printf("<td>%s</td>\n",$processingTab3["homepage"]);
                    printf("</tr>\n");

                    printf("<tr>\n");
                    printf("<th>Poster</th>");
                    printf("<td><img class=\"fit-picture\" src=\"https://image.tmdb.org/t/p/w342%s\" alt=\"Picture of the movie\"</img></td>\n",$processingTab1["poster_path"]);
                    printf("<td><img class=\"fit-picture\" src=\"https://image.tmdb.org/t/p/w342%s\" alt=\"Picture of the movie\"</img></td>\n",$processingTab2["poster_path"]);
                    printf("<td><img class=\"fit-picture\" src=\"https://image.tmdb.org/t/p/w342%s\" alt=\"Picture of the movie\"</img></td>\n",$processingTab3["poster_path"]);
                    printf("</tr></tbody>\n");

                    if(isset($_GET["language"]))
                    {
                        switch ($_GET["language"])
                        {
                            case "vf":
                                $trailerTab = tmdbget("movie/".$_GET["id"]."/videos",['language' => 'fr']);
                                $movieTitle = $processingTab3["original_title"];
                                break;
                            
                            case "ve":
                                $trailerTab = tmdbget("movie/".$_GET["id"]."/videos",['language' => 'en']);
                                $movieTitle = $processingTab2["original_title"];
                                break;
                            
                            default:
                                $trailerTab = tmdbget("movie/".$_GET["id"]."/videos");
                                $movieTitle = $processingTab1["original_title"];
                                break;
                        }
                    }
                    else
                    {
                        $trailerTab = tmdbget("movie/".$_GET["id"]."/videos");
                        $movieTitle = $processingTab1["original_title"];
                    }
                    
                    $processingTrailerTab = json_decode($trailerTab, true);
                    $trailerAvailable = false;

                    if(isset($processingTrailerTab["results"][0]["key"]))
                    {
                        for($i = 0; isset($processingTrailerTab["results"][$i]["key"]); $i++)
                            if($processingTrailerTab["results"][$i]["site"])
                                $YTlink = $processingTrailerTab["results"][$i]["key"];
                    }
                    else
                        printf("<p>Désolé, mais aucune bande d'annonce n'a été trouvée pour ".$movieTitle." dans la langue que vous avez choisie.</p>");
                    if(!isset($YTlink) && isset($processingTrailerTab["results"][0]["key"]))
                        printf("<p>Désolé, mais ".$movieTitle." n'a aucune bande d'annonce sur YouTube dans la langue que vous avez choisie.</p>");
                    if(isset($YTlink))
                        printf("<iframe width=\"420\" height=\"315\"src=\"https://www.youtube.com/embed/".$YTlink."\"></iframe>");

                }
            ?>
    </body>
</html>