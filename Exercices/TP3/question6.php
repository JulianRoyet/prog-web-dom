<?php
    require_once("tp3-helpers.php");
?>
<html>
    <head>
        <title>Question6</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../TP2/clientwebservice.css">
    </head>
    <body>
    
        <form method="get" action="question6.php">
            <label for="id">Veuillez rentrer l'identifiant d'une collection. Par exemple, l'identifiant de Star Wars est le 10, et celui du Seigneur des Anneaux le 119.</label> <input type="text" id="id" name="id"/> <br />
            <input type="submit" value="Valider"/>
        </form>
   
        <?php
            if(isset($_GET["id"]))
            {
                $iterator = tmdbget("collection/".$_GET["id"]);
                $processingIteration = json_decode($iterator, true);
                if(!isset($processingIteration["parts"][0]))
                {
                    printf("Apparamment, l'identifiant que vous avez entré n'appartient pas à une collection, ou bien cette collection est vide.\n");
                    return;
                }
                printf("<table><thread><tr><th></th>");
                printf("<th>Version originale</th>\n");
                printf("<th>Version anglaise</th>\n");
                printf("<th>Version française</th></tr></thread><tbody>\n");

                for($i = 0; isset($processingIteration["parts"][$i]); $i++)
                {
                    $movieID = $processingIteration["parts"][$i]["id"];

                    $tab1 = tmdbget("movie/".$movieID);
                    $tab2 = tmdbget("movie/".$movieID,['language' => 'en']);
                    $tab3 = tmdbget("movie/".$movieID,['language' => 'fr']);
                    
                    $processingTab1 = json_decode($tab1, true);
                    $processingTab2 = json_decode($tab2, true);
                    $processingTab3 = json_decode($tab3, true);

                    printf("<tr><td></td></tr><tr>\n");
                    printf("<th>Title</th>");
                    printf("<td>%s</td>\n",$processingTab1["title"]);
                    printf("<td>%s</td>\n",$processingTab2["title"]);
                    printf("<td>%s</td>\n",$processingTab3["title"]);
                    printf("</tr>\n");

                    printf("<tr>\n");
                    printf("<th>Original title</td>");
                    printf("<td>%s</td>\n",$processingTab1["original_title"]);
                    printf("<td>%s</td>\n",$processingTab2["original_title"]);
                    printf("<td>%s</td>\n",$processingTab3["original_title"]);
                    printf("</tr>\n");

                    
                    if(isset($processingTab1["tagline"]))
                    {
                        printf("<tr>\n");
                        printf("<th>Tagline</th>");
                        printf("<td>%s</td>\n",$processingTab1["tagline"]);
                        printf("<td>%s</td>\n",$processingTab2["tagline"]);
                        printf("<td>%s</td>\n",$processingTab3["tagline"]);
                        printf("</tr>\n");
                    }

                    printf("<tr>\n");
                    printf("<th>Overview</th>");
                    printf("<td>%s</td>\n",$processingTab1["overview"]);
                    printf("<td>%s</td>\n",$processingTab2["overview"]);
                    printf("<td>%s</td>\n",$processingTab3["overview"]);
                    printf("</tr>\n");

                    printf("<tr>\n");
                    printf("<th>Release date</th>");
                    printf("<td>%s</td>\n",$processingTab1["release_date"]);
                    printf("<td>%s</td>\n",$processingTab2["release_date"]);
                    printf("<td>%s</td>\n",$processingTab3["release_date"]);
                    printf("</tr>\n");

                    printf("<tr>\n");
                    printf("<th>Home Page</th>");
                    printf("<td>%s</td>\n",$processingTab1["homepage"]);
                    printf("<td>%s</td>\n",$processingTab2["homepage"]);
                    printf("<td>%s</td>\n",$processingTab3["homepage"]);
                    printf("</tr>\n");

                    printf("<tr>\n");
                    printf("<th>Poster</th>");
                    printf("<td><img class=\"fit-picture\" src=\"https://image.tmdb.org/t/p/w342%s\" alt=\"Picture of the movie\"</img></td>\n",$processingTab1["poster_path"]);
                    printf("<td><img class=\"fit-picture\" src=\"https://image.tmdb.org/t/p/w342%s\" alt=\"Picture of the movie\"</img></td>\n",$processingTab2["poster_path"]);
                    printf("<td><img class=\"fit-picture\" src=\"https://image.tmdb.org/t/p/w342%s\" alt=\"Picture of the movie\"</img></td>\n",$processingTab3["poster_path"]);
                    printf("</tr>\n");
                }
            }    
        ?>
    </body>
</html>