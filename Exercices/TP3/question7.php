<html>
    <head>
        <title>Question7</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../TP2/clientwebservice.css">
    </head>
    <body>
        <?php
            // NOTE: This can definitly be improved and automated for any movie, but I'll do it when if I have the time to
            require_once("tp3-helpers.php");
            printf("<table><thread><tr><th>Acteur</th>\n<th>Rôle</th>\n<th>Nombre total de films joués dans cette trilogie</th><th>Premier film de ce rôle</th>\n</tr></thread><tbody>");

            $tab1 = tmdbget("movie/120/casts");
            $tab2 = tmdbget("movie/121/casts");
            $tab3 = tmdbget("movie/122/casts");

            $processingTab1 = json_decode($tab1, true);
            $processingTab2 = json_decode($tab2, true);
            $processingTab3 = json_decode($tab3, true);

            for($i = 0; isset($processingTab1["cast"][$i]); $i++)
            {
                $fstMovieName = $processingTab1["cast"][$i]["name"];
                $fstMovieChar = $processingTab1["cast"][$i]["character"];

                $isSnd = false;
                $isTrd = false;
                for($j = 0; isset($processingTab2["cast"][$j]); $j++)
                {
                    $sndMovieName = $processingTab2["cast"][$j]["name"];
                    if($fstMovieName == $sndMovieName)
                    {
                        $isSnd = true;
                        $sndMovieChar = $processingTab2["cast"][$j]["character"];
                        // Removing the actor in order to have only actors that were not currenty used yet
                        array_splice($processingTab2["cast"],$j,1);
                        break;
                    }
                }
                for($j = 0; isset($processingTab3["cast"][$j]); $j++)
                {
                    $trdMovieName = $processingTab3["cast"][$j]["name"];
                    if($fstMovieName == $trdMovieName)
                    {
                        $isTrd = true;
                        $trdMovieChar = $processingTab3["cast"][$j]["character"];
                        array_splice($processingTab3["cast"],$j,1);
                        break;
                    }
                }
                $numOfMovies = 1;
                if($isSnd)
                    $numOfMovies++;
                if($isTrd)
                    $numOfMovies++;
                printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%d</td>\n<td>La communauté de l'anneau</td>\n</tr>\n",$fstMovieName, $fstMovieChar, $numOfMovies);
                // On ne veut pas de rôles en doublon
                if($isSnd && $sndMovieChar != $fstMovieChar)
                    printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%d</td>\n<td>Les deux tours</td>\n</tr>\n",$sndMovieName, $sndMovieChar, $numOfMovies);
                if($isTrd && $trdMovieChar != $fstMovieChar && $trdMovieChar != $sndMovieChar)
                    printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%d</td>\n<td>Le retour du roi</td>\n</tr>\n",$trdMovieName, $trdMovieChar, $numOfMovies);
            }

            // Actors that were not in the first movie
            for($i = 0; isset($processingTab2["cast"][$i]); $i++)
            {

                $sndMovieName = $processingTab2["cast"][$i]["name"];
                $sndMovieChar = $processingTab2["cast"][$i]["character"];

                $numOfMovies = 1;

                for($j = 0; isset($processingTab3["cast"][$j]); $j++)
                {
                    $trdMovieName = $processingTab3["cast"][$j]["name"];
                    if($sndMovieName == $trdMovieName)
                    {
                        $numOfMovies++;
                        $trdMovieChar = $processingTab3["cast"][$j]["character"];
                        array_splice($processingTab3["cast"],$j,1);
                        break;
                    }
                }
                    printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%d</td>\n<td>Les deux tours</td>\n</tr>\n",$sndMovieName, $sndMovieChar, $numOfMovies);
                        
                    if($numOfMovies == 2 && $trdMovieChar != $sndMovieChar)
                    printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%d</td>\n<td>Le retour du roi</td>\n</tr>\n",$trdMovieName, $trdMovieChar, $numOfMovies);
            }

            // Actors that were only in the last film
            for($i = 0; isset($processingTab3["cast"][$i]); $i++)
            {
                $trdMovieName = $processingTab3["cast"][$i]["name"];
                $trdMovieChar = $processingTab3["cast"][$i]["character"];

                printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>1</td>\n<td>Le retour du roi</td>\n</tr>\n",$trdMovieName, $trdMovieChar);

            }
        ?>

    </body>
</html>