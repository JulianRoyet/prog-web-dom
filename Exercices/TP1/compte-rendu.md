% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : 

## Participants 

* Théo Capet
* Julian Royet
* Samuel Conjard


## Calculs

GET est mieux pour récupérer une resource et POST est mieux pour envoyer des données au serveur

/!\ argv[0] n'est pas le premier arg

## CSS

Plus profond: Saisons: profondeur 5

## Table de multiplication

Il faut mettre la propriété border-collapse dans le css pour les cases soient plus jolies.
Pas de difficulté particulière.

## Analyse des caractères unicode

Des chiffres et des lettres.

## Calendrier

Utilisation de nouvelle fonctionq. Quelques difficultés pour mettre les chiffres dans les bonnes cases (notamment trouver une formule explicite à l'aide de modulo).


## Analyse (parsing) d’un document HTML

3) L'une des façons de respecter la hiérarchie des balises nécessite le stockage de ces dernières avec la conserveration de leur ordre d'apparition.
L'un des moyens pour y arriver est celui d'utiliser une forme de tableau contenant les balises dans le bon ordre, ainsi qu'un tableau par balise
que l'on cherche à afficher. Dans le cas présent, il s'agit de deux tableaux, un pour le contenu des balises h2, et un second pour le contenu des
balises h3. Il suffit ensuite de conserver une trace de quelles parties des deux tableaux ont été utilisées, et d'accéder à tel ou tel tableau
selon quelle balise le tableau principal fait référence.

    Cette solution n'est pas plus compliquée à proprement parler que l'implémentation de notre code jusque lors, affichant simplement le contenu
d'un tableau dans lequel tout le contenu des différentes balises h2 était entreposé, cependant qu'elle nécessite une bonne connaissance de la
classe DOMDocument.

4) Consulter la référence de l'énoncé menant vers le forum StackOverflow permet de se faire une bonne idée de la faisabilité d'une telle entreprise.