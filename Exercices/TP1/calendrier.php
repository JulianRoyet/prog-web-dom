<html>
    <head>
        <title>Calendrier</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="calendrier.css">
    </head>

    <body>
        <table>
            <thread>
                <tr>
                    <?php
                        printf("<th>Numéro</th>\n");
                        printf("<th>Nom</th>\n");
                        printf("<th>A faire</th>\n");
                    ?>
                </tr>
            </thread>
            <tbody>
                <?php
                    $nom_jour = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
                    $mois = array("Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre");
                    $months = array("January","February","March","April","May","June","July","August","September","October","November","December");
                    if(!isset($_POST["mois"]) && !isset($_POST["annee"])){
                        $jour = getdate()["mday"];
                        $sjour = getdate()["wday"];
                        for ($k=1 ; $k<=$jour ; $k++){
                            $indice_nom = ($sjour+$k-1)%7;
                            printf("<tr>\n");
                            printf("<th>%d</th>\n", $k);
                            printf("<th>%s</th>\n", $nom_jour[$indice_nom]);
                            printf("<th></th>\n");
                            printf("</tr>\n");
                        }
                        $k=$jour+1;
                        while (checkdate(getdate()["mon"], $k, getdate()["year"])){
                            $indice_nom = ($sjour+$k-1)%7;
                            printf("<tr>\n");
                            printf("<th>%d</th>\n", $k);
                            printf("<th>%s</th>\n", $nom_jour[$indice_nom]);
                            printf("<th></th>\n");
                            printf("</tr>\n");
                            $k++;
                        }
                    }
                    else{
                        $k = 1;
                        $int_mois = $_POST["mois"];
                        $annee = $_POST["annee"];
                        while(checkdate($int_mois, $k, $annee)){
                            $tmp = strtotime(sprintf("%s %d %d 00:00:00 UTC", $months[$int_mois-1], $k,  $annee));
                            $sjour = getdate($tmp)["wday"];
                            $indice_nom = ($sjour+$k-1)%7;
                            printf("<tr>\n");
                            printf("<th>%d</th>\n", $k);
                            printf("<th>%s</th>\n", $nom_jour[$indice_nom]);
                            printf("<th></th>\n");
                            printf("</tr>\n");
                            $k++;
                        }
                    }
                ?>
            </tbody>
        </table>

        <form method="post" action="calendrier.php">
            <label for="mois">Mois</label> <input type="text" id="mois" name="mois"/> <br />
            <label for="annee">Année</label> <input type="text" id="annee" name="annee"/> <br />
            <input type="submit" value="submit"/>
        </form>

    </body>


</html>