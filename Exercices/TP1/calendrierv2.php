<html>
    <head>
        <title>Calendrier</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="calendrier.css">
    </head>

    <body>
        <table>
            <thread>
                <tr>
                    <?php
                        printf("<th>Lundi</th>\n");
                        printf("<th>Mardi</th>\n");
                        printf("<th>Mercredi</th>\n");
                        printf("<th>Jeudi</th>\n");
                        printf("<th>Vendredi</th>\n");
                        printf("<th>Samedi</th>\n");
                        printf("<th>Dimanche</th>\n");
                    ?>
                </tr>
            </thread>
            <tbody>
                <?php
                    $nom_jour = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
                    $months = array("January","February","March","April","May","June","July","August","September","October","November","December");
                    $ev_j = 0;
                    $ev_t = "";
                    if(isset($_POST["jour"]) && isset($_POST["event"])){
                        $ev_j = $_POST["jour"];
                        $ev_t = $_POST["event"];
                    }
                    $sjour = getdate()["wday"];
                    $int_mois = getdate()["mon"];
                    $tmp = strtotime(sprintf("%s %d %d 00:00:00 UTC", $months[$int_mois-1], 1,  getdate()["year"]));
                    $sjour = getdate($tmp)["wday"];
                    for ($i = 0 ; $i < 5 ; $i++){
                        printf("<tr>\n");
                        for ($k=0 ; $k<7 ; $k++){
                            $id = 7*$i+$k;
                            $rel = $id - $sjour;
                            $timstamp_case = $tmp + 86400*$rel;
                            $jour = getdate($timstamp_case)["mday"];
                            $mois_case = getdate($timstamp_case)["mon"];

                            if($int_mois == $mois_case){
                                printf("<td>\n");
                                printf("%d\n", $jour);
                                
                                if(strlen($ev_t) > 0 && $ev_j == $rel+1){
                                    printf("<div>%s</div>", $ev_t);
                                }
                                
                                printf("</td>\n");
                                }
                            else{
                                printf("<td class=\"disabled\">\n");
                                printf("%d\n", $jour);
                                printf("</td>\n");
                            }
                            
                        }
                        printf("</tr>\n");
                    }
                    
                ?>
            </tbody>
        </table>

        <form method="post" action="calendrierv2.php">
            <label for="jour">Jour</label> <input type="text" id="jour" name="jour"/> <br />
            <label for="event">Evènement</label> <input type="text" id="event" name="event"/> <br />
            <input type="submit" value="submit"/>
        </form>

    </body>
</html>