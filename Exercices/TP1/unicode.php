<html>
    <head>
        <title>Unicode</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="unicode.css">
    </head>

    <body>
        <div class="container">
            <?php
                if (isset($_GET["mot"])){
                    $character = $_GET["mot"];
                    $code = mb_ord($character);
                    $tmp = $code-$code%16;
                    for ($k=0 ; $k<16 ; $k++){
                        $commande = "unicode -d " . ($tmp+$k) . " | head -n 1 | cut -c 8-";
                        $label = exec($commande);
                        if ($tmp+$k == $code){
                            printf("<div class=\"lettre highlight\" title=\"%s\">%s", $label, mb_chr($tmp + $k));
                        }
                        else{
                            printf("<div class=\"lettre\" title=\"%s\">%s", $label,  mb_chr($tmp + $k));
                        }
                        printf("<a class=\"code\" href=\"https://util.unicode.org/UnicodeJsps/character.jsp?a=%04x\">", $tmp+$k);
                        printf("U+%04x</a></div>", $tmp+$k);
                    }
                }
                else{
                    printf("veuillez saisir un mot");
                }
            ?>
        </div>
        <form method="get" action="unicode.php">
            <label for="mot">MOT</label> <input type="text" id="mot" name="mot"/> <br />
            <input type="submit" value="mot"/>
        </form>
    </body>
</html>