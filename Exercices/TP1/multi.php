<html>
    <head>
        <title>Un truc de calcul</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="multi.css">
    </head>

    <body>
        <table>
            <thread>
                <tr>
                    <th></th>
                <?php
                    for($i=1; $i<=10; $i++){
                        printf("<th>%s</th>\n", $i);
                    }
                ?>
                </tr>
            </thread>
            <tbody>
                <?php
                    $ligne = -1;
                    if (isset($_POST["highlight"])){
                        $ligne = $_POST["highlight"];
                    }
                    for($i=1; $i<=10; $i++){
                        if ($i == $ligne){
                            printf("<tr class=\"highlight\">\n");
                        }
                        else{
                            printf("<tr>\n");
                        }
                        printf("<th>%s</th>\n", $i);
                        
                        for($j=1; $j<=10; $j++){
                            printf("<td>%s</td>", $i*$j);
                        }

                        printf("</tr>\n");
                    }
                ?>
            </tbody>
        </table>

        <form method="post" action="multi.php">
            <label for="highlight-text">Highlight</label> <input type="text" id="highlight-text" name="highlight"/> <br />
            <input type="submit" value="surligner"/>
        </form>
    </body>
</html>


