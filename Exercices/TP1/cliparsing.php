
<?php
if ($argc != 2)
{
    echo "Usage : php cliparsing.php <url>", PHP_EOL;
    return;
}
$doc = new DOMDocument();
libxml_use_internal_errors(true);
$request = $doc->loadHTMLFile($argv[1]);

if($request)
{
    $doc->loadHTMLFile($argv[1]);
    $xpath = new DOMXpath($doc);

    $h2s = $doc->getElementsByTagName('h2');
    $h3s = $doc->getElementsByTagName('h3');
    $h2Count = 0;
    $h3Count = 0;
    
    $tagsArray = $xpath->query('//h2 | //h3');
    
    $length = $tagsArray->length;
    
    for ($i = 0; $i < $length; $i++)
    {
        $element = $tagsArray->item($i);
    
        if ($element->tagName == 'h2')
        {
            echo $h2s[$h2Count]->nodeValue;
            echo PHP_EOL;
            $h2Count++;
        } else {
            echo $h3s[$h3Count]->nodeValue;
            echo PHP_EOL;
            $h3Count++;
        }
    }
}
else
{
    echo "The url could not be read.", PHP_EOL;
}
?>
