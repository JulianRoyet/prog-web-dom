<html>
    <head>  
        <title>Calendrier</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="calendrier.css">
    </head>

    <label for="start">Start date:</label>

<input type="date" id="start" name="trip-start"
       value=<?php sprintf("%d-%d-%d", getdate()["year"], getdate()["mon"], getdate()["mday"]) ?>
       min=<?php sprintf("%d-01-01", getdate()["year"]) ?> max=<?php sprintf("%d-01-01", getdate()["year"]) ?>>
</html>